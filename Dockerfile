FROM python:3.8.2
ENV LANG C.UTF-8

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1

RUN apt-get update -y && \
    apt-get install -y apt-transport-https rsync gettext libgettextpo-dev && \
#    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
#    apt-get install -y nodejs &&\
    rm -rf /var/lib/apt/lists/*

RUN pip install "gunicorn>=19.8,<19.9"

# Install Python requirements.
COPY requirements.txt .
RUN pip install -r requirements.txt

# Copy application code.
COPY . .

RUN adduser --disabled-password myuser

COPY ./entrypoint.sh /entrypoint.sh
RUN sed -i 's/\r//' /entrypoint.sh
RUN chmod +x /entrypoint.sh
RUN chown myuser /entrypoint.sh
USER myuser

CMD gunicorn backend.wsgi:application