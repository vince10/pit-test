from datetime import timedelta

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.urls import reverse_lazy

from .forms import RegistrationForm, LoginForm,CustomerForm
from .models import LoginAttempt, User, Customer
from .token import account_activation_token
from .decorators import unauthenticated_user
from .utils import send_user_email
from itertools import chain
# Create your views here.
response = {}


@login_required
def render_table(request):
    context = {}
    fields = [field.name for field in Customer._meta.get_fields()]
#     fields = list(set(chain.from_iterable(
#     (field.name, field.attname) if hasattr(field, 'attname') else (field.name,)
#     for field in Customer._meta.get_fields()
#     if not (field.many_to_one and field.related_model is None)
# )))
    fields = fields[1:]
    all_customers = Customer.objects.all()
    context['customers'] = all_customers
    context['fields'] = fields
    print('all customers:\n',all_customers)
    print('all fields:\n', fields)

    html = 'customerTable.html'
    return render(request,html,context)
# def home(request):
#     return render(request,"base.html")
# def registration(request):
#     html = "register.html"
#     global response
#     # context = {}
#     if request.POST:
#         form = RegistrationForm(request.POST)
#         if form.is_valid():
#
#             user_created = form.save(commit=False)
#             user_created.is_active = True
#             user_created.save()
#             print('user created:\n',user_created)
#             print('All accounts:\n',Account.objects.all())
#
#             email    = form.cleaned_data.get('email')
#             raw_pass = form.cleaned_data.get('password1')
#             account = authenticate(email=email, password = raw_pass)
#             login(request, account)
#             messages.success(request, "You have been Registered as {}".format(request.user.username))
#             return redirect("customertable:table")
#         else:
#             messages.error(request, "Please Correct Below Errors")
#             response['registration_form'] = form
#     else:
#         form = RegistrationForm()
#         response['registration_form'] = form
#     return render(request,html, response)

# def logout_view(request):
#     logout(request)
#     messages.success(request, "Logged Out Successful")
#     return redirect("customertable:table")

# def  login_view(request):
#
#     # context = {}
#     html = "login.html"
#     global response
#     user = request.user
#     if user.is_authenticated:
#         return redirect("customertable:table")
#     if request.POST:
#         form    = AccountAuthenticationForm(request.POST)
#         email   = request.POST.get('email')
#         password = request.POST.get('password')
#         user =  authenticate(email=email, password=password)
#         print('usernya:')
#         print(user)
#         if user:
#             login(request, user)
#             messages.success(request, "Logged In")
#             return redirect("customertable:table")
#         else:
#             messages.error(request,"please Correct Below Errors")
#     else:
#         form = AccountAuthenticationForm()
#     response['login_form'] = form
#     return render(request, html, response)

def login_n(request):
    html = 'login.html'
    return render(request,html)

# def account_view (request):
#     """
#       Renders userprofile page "
#     """
#     global response
#     if not request.user.is_authenticated:
#         return redirect("customertable:login")
#     # context = {}
#     if request.POST:
#         form = AccountUpdateform(request.POST, instance = request.user)
#         if form.is_valid():
#             form.save()
#             messages.success(request, "profile Updated")
#         else:
#             messages.error(request, "Please Correct Below Errors")
#     else:
#         form  = AccountUpdateform(
#             initial={
#             'email':request.user.email,
#             'username':request.user.username,
#             }
#         )
#     response['account_form']=form
#     html = "userprofile.html"
#
#     return render(request, html,response)
def register(request):
    html = 'register.html'
    return render(request,html)

@login_required
def customer_form(request):
    context = {}
    if request.POST:
        form = CustomerForm(request.POST)
        if form.is_valid():
            customer = form.save()
            print('customer added:\n',customer)
            return redirect('customertable:home')
        else:
            messages.error(request,"Can't add customer, please refill the form again")
    html = 'forms.html'
    context['customer_form'] = CustomerForm
    return render(request,html,context)

@unauthenticated_user
def signup_page(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            to_email = form.cleaned_data.get('email')
            current_site = get_current_site(request)
            mail_subject = 'Activate your account'
            response = send_user_email(user, mail_subject, to_email, current_site, 'email_verification.html')
            if response == 'success':
                messages.success(request, "We have sent you an activation link in your email. Please confirm your"
                                          "email to continue. Check your spam folder if you don't receive it")
                print('messagesnya:\n',list(messages.get_messages(request)))
                print('messagesnya:\n', len(list(messages.get_messages(request))))
            else:
                messages.error(request, 'An error occurred. Please ensure you have good internet connection and you have entered a valid email address')
                user.delete()
        else:
            if form.errors:
                for field in form:
                    for error in field.errors:
                        messages.error(request, error)
            form = RegistrationForm()

    context = {
        'form': form
    }

    html="register.html"
    return render(request, html, context)
@unauthenticated_user
def login_page(request):
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            now = timezone.now()
            try:
                _user = User.objects.get(email=email)
                login_attempt, created = LoginAttempt.objects.get_or_create(user=_user)  # get the user's login attempt
                if (login_attempt.timestamp + timedelta(seconds=settings.LOGIN_ATTEMPTS_TIME_LIMIT)) < now:
                    user = authenticate(request, username=email, password=password)
                    if user is not None:
                        login(request, user)
                        login_attempt.login_attempts = 0    # reset the login attempts
                        login_attempt.save()
                        return redirect(settings.LOGIN_REDIRECT_URL)  # change expected_url in your project

                    else:
                        # if the password is incorrect, increment the login attempts and
                        # if the login attempts == MAX_LOGIN_ATTEMPTS, set the user to be inactive and send activation email
                        login_attempt.login_attempts += 1
                        login_attempt.timestamp = now
                        login_attempt.save()
                        if login_attempt.login_attempts == settings.MAX_LOGIN_ATTEMPTS:
                            _user.is_active = False
                            _user.save()
                            # send the re-activation email
                            mail_subject = "Account suspended"
                            current_site = get_current_site(request)
                            send_user_email(_user, mail_subject, email, current_site, 'email_account_suspended.html')
                            messages.error(request, 'Account suspended, maximum login attempts exceeded. '
                                                    'Reactivation link has been sent to your email')
                        else:
                            messages.error(request, 'Incorrect email or password')
                        return redirect(settings.LOGIN_URL)
                else:
                    messages.error(request, 'Login failed, please try again')
                    return redirect(settings.LOGIN_URL)

            except ObjectDoesNotExist:
                messages.error(request, 'Incorrect email or password')
                return redirect(settings.LOGIN_URL)
        else:
            if form.errors:
                for field in form:
                    for error in field.errors:
                        messages.error(request, error)
    context = {'form': form}
    html = 'login.html'
    return render(request, html, context)
def activate_account_page(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        # print('uid: ',uid)
        user = User.objects.get(pk=uid)
        # print('tryyyy')
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
        # print('exceptttt')
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login_attempt, created = LoginAttempt.objects.get_or_create(user=user)
        if login_attempt.login_attempts >= settings.MAX_LOGIN_ATTEMPTS:
            login_attempt.login_attempts = 0
            login_attempt.save()
            messages.success(request, 'Account restored, you can now proceed to login')
        else:
            messages.success(request, 'Thank you for confirming your email. You can now login.')
        return redirect(settings.LOGIN_REDIRECT_URL)
    else:
        messages.error(request, 'Activation link is invalid!')
        return redirect(settings.LOGIN_URL)

def logout_view(request):
    logout(request)
    return redirect(settings.LOGOUT_REDIRECT_URL)




