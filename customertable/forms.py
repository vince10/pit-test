from .models import *
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
# from .models import Account
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField



class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = "__all__"

        STATUS_CHOICES = [
            ('Single','Single'),
            ('Married', 'Married')
        ]

        widgets = {
            'status':forms.Select(choices=STATUS_CHOICES,attrs={'class':'form-control','placeholder':'Marriage Status'}),
            'income':forms.NumberInput(attrs={'placeholder':'Income in IDR'}),
            'address': forms.Textarea(),

        }

User = get_user_model()


class UserAdminCreationForm(forms.ModelForm):

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email',)

    def clean_password2(self): # checking that the two passwords match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords do not match')
        return password2

    def save(self, commit=True): # save he proovided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user




class LoginForm(forms.Form):
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-input',
                                                                           'placeholder': 'Enter email'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-input',
                                                                                   'placeholder': 'Enter password'}))


class RegistrationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-input', 'placeholder': 'Enter password'}))
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-input', 'placeholder': 'Confirm password'}))

    class Meta:
        model = User
        fields = ['email']
        widgets = {'email': forms.EmailInput(attrs={'class': 'form-input', 'placeholder': 'Enter Email'})}

    def clean_password2(self):  # checking that the two passwords match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords do not match')
        return password2

    def save(self, commit=True): # save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.is_active = False
            user.save()
        return user

class UserAdminChangeForm(forms.ModelForm):

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'is_active', 'admin')

    def clean_password(self):

        return self.initial['password']



# class RegistrationForm(UserCreationForm):
#
#     email = forms.EmailField(max_length=60, help_text = 'Required. Add a valid email address')
#     class Meta:
#         model = Account
#         fields = ['email', 'username', 'password1', 'password2']
#
#     def __init__(self, *args, **kwargs):
#
#         super(RegistrationForm, self).__init__(*args, **kwargs)
#         field_collections = [self.fields['email'],self.fields['username'],self.fields['password1'],self.fields['password2']]
#         for field in field_collections:
#             field.widget.attrs.update({'class': 'form-control '})
#
# class AccountAuthenticationForm(forms.ModelForm):
#
#     password  = forms.CharField(label= 'Password', widget=forms.PasswordInput)
#
#     class Meta:
#         model  =  Account
#         fields =  ['email', 'password']
#         widgets = {
#                    'email':forms.TextInput(attrs={'class':'form-control'}),
#                    'password':forms.TextInput(attrs={'class':'form-control'}),
#         }
#     def __init__(self, *args, **kwargs):
#         super(AccountAuthenticationForm, self).__init__(*args, **kwargs)
#         field_collections = [self.fields['email'],self.fields['password']]
#         for field in field_collections :
#             field.widget.attrs.update({'class': 'form-control '})
#
#     def clean(self):
#         if self.is_valid():
#
#             email = self.cleaned_data.get('email')
#             password = self.cleaned_data.get('password')
#             if not authenticate(email=email, password=password):
#                 raise forms.ValidationError('Invalid Login')
#
# class AccountUpdateform(forms.ModelForm):
#
#     class Meta:
#         model  = Account
#         fields = ['email', 'username']
#         widgets = {
#                    'email':forms.TextInput(attrs={'class':'form-control'}),
#                    'password':forms.TextInput(attrs={'class':'form-control'}),
#         }
#
#     def __init__(self, *args, **kwargs):
#
#         super(AccountUpdateform, self).__init__(*args, **kwargs)
#         field_collections = [self.fields['email'],self.fields['username']]
#         for field in field_collections:
#             field.widget.attrs.update({'class': 'form-control '})
#
#     def clean_email(self):
#         if self.is_valid():
#             email = self.cleaned_data['email']
#             try:
#                 account = Account.objects.exclude(pk = self.instance.pk).get(email=email)
#             except Account.DoesNotExist:
#                 return email
#             raise forms.ValidationError("Email '%s' already in use." %email)
#     def clean_username(self):
#         if self.is_valid():
#             username = self.cleaned_data['username']
#             try:
#                 account = Account.objects.exclude(pk = self.instance.pk).get(username=username)
#             except Account.DoesNotExist:
#                 return username
#
