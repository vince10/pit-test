from django.apps import AppConfig


class CustomertableConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'customertable'
