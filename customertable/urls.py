from django.urls import path
from .views import render_table,login_n,register,customer_form,signup_page, login_page,  logout_view,activate_account_page

urlpatterns = [
    # path('',render_table,name='home'),
    path('',render_table,name='home'),
    path('login/',login_page,name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', signup_page, name='register'),
    path('input-customer/',customer_form,name='input_customer'),
    path('activate/<slug:uidb64>/<slug:token>/',activate_account_page,name='activate'),

    #
    # path('register',registration, name="register"),
    # path('logout',logout_view, name="logout"),
    # path('login',login_view, name="login"),
    # path('home',home, name="home"),
    # path('profile',account_view, name="account"),
]