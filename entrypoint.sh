#!/bin/sh
python3 manage.py collectstatic --noinput
python3 manage.py compress --force
python3 manage.py makemigrations --noinput
python3 manage.py migrate
python3 manage.py initadmin
gunicorn backend.wsgi --workers 3 --bind 0.0.0.0:8000 --log-level info